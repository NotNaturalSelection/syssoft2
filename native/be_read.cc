#include "be_read.h"
#include <stdint.h>
#include <byteswap.h>
#include <unistd.h>

void read_uint64(uint64_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE64);
    *ptr = bswap_64(*ptr);
}

void read_uint32(uint32_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE32);
    *ptr = bswap_32(*ptr);
}

void read_uint16(uint16_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE16);
    *ptr = bswap_16(*ptr);
}

void read_uint8(uint8_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE8);
}

void read_char(char *ptr, uint64_t fd) {
    read(fd, ptr, SIZE8);
}

void read_int64(int64_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE64);
    *ptr = bswap_64(*ptr);
}

void read_int32(int32_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE32);
    *ptr = bswap_32(*ptr);
}

void read_int16(int16_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE16);
    *ptr = bswap_16(*ptr);
}

void read_int8(int8_t *ptr, uint64_t fd) {
    read(fd, ptr, SIZE8);
}