#ifndef SOFTWARE1LIB_BTREE_H
#define SOFTWARE1LIB_BTREE_H

#include "node.h"

typedef struct {
    uint64_t fd;
    data_node *header_node;
    data_node **leaf_nodes;
    uint32_t leaf_nodes_num;
    uint32_t no_leaf_nodes_num;
    uint32_t btree_offset;
} btree;

typedef struct {
    uint16_t key_length;
    uint32_t parent_id;
    uint16_t sym_length;
    char *node_name;
} catalog_key;

enum {
    FolderRecord = 1,
    FileRecord = 2,
    kHFSPlusFolderThreadRecord = 3,
    kHFSPlusFileThreadRecord = 4
};

void free_key(catalog_key *ptr);

uint32_t set_value_ptr_by_id(btree *btree_ptr, uint32_t item_id, catalog_key *key_to_fill);

uint32_t get_children_of_id(btree *btree_ptr, uint32_t item_id, char **buffer);

int32_t get_value_offset_by_key(char *key_to_comp, btree *btree_ptr, uint32_t parent_id, int16_t *type);

btree *init_btree(uint64_t fd, uint32_t btree_offset);

#endif //SOFTWARE1LIB_BTREE_H
