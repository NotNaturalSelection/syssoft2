#include <nan.h>
#include "hfs/hfs.h"
#include "partitions/partition.h"
#include <malloc.h>
#include <string.h>

#define PARTS_BUF_SIZE 50
using namespace v8;

static partition parts[PARTS_BUF_SIZE];
static hfsplus* fs;

void LoadPartitions(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    uint64_t total = load_partitions(parts, PARTS_BUF_SIZE);
    char output[10000] = {0};
    char buf[100] = {0};
    sprintf(buf, "MAJ:MIN| Size(Mb) Name\n");
    strcat(output, buf);
    for(uint64_t i = 0; i < total; i++) {
        sprintf(buf, "%3d:%3d| %8lu %s\n", parts[i].major, parts[i].minor, parts[i].size_mb, parts[i].name);
        strcat(output, buf);
    }
    output[strlen(output)-1] = '\0';
    info.GetReturnValue().Set(Nan::New(output).ToLocalChecked());
}

void InitHFSPlus(const Nan::FunctionCallbackInfo<Value>& info) {
    Isolate* isolate = info.GetIsolate();
    String::Utf8Value str(isolate, info[0]);
    fs = read_hfsplus(*str);
    if(ERRNO_HFS == NO_RESERVED_SPACE) {
        isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "It's not an HFS+ filesystem. No reserved space").ToLocalChecked()));
        return;
    }
    if(ERRNO_HFS == BAD_ROOT_STRUCTURE) {
        isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "It's not an HFS+ filesystem. Bad root structure").ToLocalChecked()));
        return;
    }
}

void Pwd(const Nan::FunctionCallbackInfo<Value>& info) {
    info.GetReturnValue().Set(Nan::New(fs->pwd).ToLocalChecked());
}

void LS(const Nan::FunctionCallbackInfo<Value>& info) {
    char* buffer[1000];
    uint32_t total = ls(fs, buffer);
    char output[10000] = {0};
    char buf[100] = {0};
    for(uint64_t i = 0; i < total; i++) {
        sprintf(buf, "%s\n", buffer[i]);
        strcat(output, buf);
    }
    output[strlen(output)-1] = '\0';
    info.GetReturnValue().Set(Nan::New(output).ToLocalChecked());
}

void CD(const Nan::FunctionCallbackInfo<Value>& info) {
    Isolate* isolate = info.GetIsolate();
    String::Utf8Value str(isolate, info[0]);
    ERRNO_HFS = NO_ERROR;
    cd(fs, *str);
    if(ERRNO_HFS == NO_SUCH_FILE_OR_DIR) {
        info.GetReturnValue().Set(Nan::New("No such directory").ToLocalChecked());
    } else if(ERRNO_HFS == IS_FILE) {
        info.GetReturnValue().Set(Nan::New("It's file").ToLocalChecked());
    } else {
        info.GetReturnValue().Set(Nan::New("Going to...").ToLocalChecked());
    }
}

void Back(const Nan::FunctionCallbackInfo<Value>& info) {
    char* pwd = back(fs);
    if (pwd == NULL && ERRNO_HFS == ALREADY_ON_TOP) {
        info.GetReturnValue().Set(Nan::New("You're in root folder").ToLocalChecked());
    } else {
        info.GetReturnValue().Set(Nan::New("Going back...").ToLocalChecked());
    }
}

void Copy(const Nan::FunctionCallbackInfo<Value>& info) {
    Isolate* isolate = info.GetIsolate();
    String::Utf8Value name(isolate, info[0]);
    String::Utf8Value destination(isolate, info[1]);
    uint32_t res = copy(*name, *destination, fs);
    if(res < 0) {
        info.GetReturnValue().Set(Nan::New("Something went wrong").ToLocalChecked());
    } else {
        info.GetReturnValue().Set(Nan::New("Done").ToLocalChecked());
    }
}
void Init(v8::Local<v8::Object> exports) {
    v8::Local<v8::Context> context = exports->CreationContext();
    exports->Set(context,
                     Nan::New("loadPartitions").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(LoadPartitions)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("initHFSPlus").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(InitHFSPlus)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("pwd").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(Pwd)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("ls").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(LS)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("cd").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(CD)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("backToParent").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(Back)
                             ->GetFunction(context)
                             .ToLocalChecked());
    exports->Set(context,
                     Nan::New("copyDirOrFile").ToLocalChecked(),
                     Nan::New<v8::FunctionTemplate>(Copy)
                             ->GetFunction(context)
                             .ToLocalChecked());
}


NODE_MODULE(hello, Init)
