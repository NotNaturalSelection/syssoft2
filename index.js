const nativehfs = require('bindings')('nativehfs')
const readline = require('readline-sync')

const EXIT_COMMANDS = ["q", "quit", "exit"]
const HELP_MESSAGE = "List of commands:\n" +
    "ls\n" +
    "cd <to>\n" +
    "pwd\n" +
    "back\n" +
    "copy <from> <to>\n" +
    "help"

if (process.argv.length < 3) {
    console.log("Too few arguments. Use \'node index.js [-l|-f] [<pathToFS>]\'")
} else {
    if(process.argv[2] === "-l") {
        console.log(nativehfs.loadPartitions())
    } else if (process.argv[2] === "-f" && process.argv.length >=4) {
        console.log("Opening HFS+ on "+process.argv[3])
        try {
            nativehfs.initHFSPlus(process.argv[3])
        } catch (e) {
            console.error(e.message)
            return;
        }
        let line = readline.question(nativehfs.pwd()+"$: ")

        while (!EXIT_COMMANDS.includes(line)) {
            let command = line.split(" ")[0]
            switch (command) {
                case "ls":
                    console.log(nativehfs.ls())
                    break;
                case "cd":
                    if (line.split(" ").length < 2) {
                        console.log("Wrong number of arguments")
                    } else {
                        console.log(nativehfs.cd(line.split(" ")[1]))
                    }
                    break;
                case "pwd":
                    console.log("You're in \'" + nativehfs.pwd() + "\'")
                    break;
                case "back":
                    console.log(nativehfs.backToParent())
                    break;
                case "copy":
                    if (line.split(" ").length < 3) {
                        console.log("Wrong number of arguments")
                    } else {
                        console.log(nativehfs.copyDirOrFile(line.split(" ")[1], line.split(" ")[2]))
                    }
                    break;
                case "":
                    break;
                case "help":
                    console.log(HELP_MESSAGE)
                    break;
                default:
                    console.log("Wrong command '" + line + "'\n" + HELP_MESSAGE)
            }
            line = readline.question(nativehfs.pwd() + "$: ")
        }
    } else {
        console.log("'-f' mode requires path to HFS+ file system as 4th argument")
    }
}

