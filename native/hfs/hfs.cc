#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <ftw.h>
#include "hfs.h"
#include "../be_read.h"

#define NULL_TERM '\0'
#define RESERVED1 1024
#define ROOT_FOLDER_ID 1

ERROR ERRNO_HFS = NO_ERROR;

fork_data *read_fork_data(uint64_t fd) {
    fork_data *res = (fork_data*) malloc(sizeof(fork_data));
    read_uint64(&res->logicalSize, fd);
    read_uint32(&res->clumpSize, fd);
    read_uint32(&res->totalBlocks, fd);
    for (int i = 0; i < 8; ++i) {
        read_uint32(&res->extents[i].startBlock, fd);
        read_uint32(&res->extents[i].blockCount, fd);
    }
    return res;
}

volume_header *read_volume_header(uint64_t fd) {
    volume_header *res = (volume_header*) malloc(sizeof(volume_header));
    read_uint16(&res->signature, fd);
    read_uint16(&res->version, fd);

    read_uint32(&res->attributes, fd);
    read_uint32(&res->lastMountedVersion, fd);
    read_uint32(&res->journalInfoBlock, fd);
    read_uint32(&res->createDate, fd);
    read_uint32(&res->modifyDate, fd);
    read_uint32(&res->backupDate, fd);
    read_uint32(&res->checkedDate, fd);
    read_uint32(&res->fileCount, fd);
    read_uint32(&res->folderCount, fd);
    read_uint32(&res->blockSize, fd);
    read_uint32(&res->totalBlocks, fd);
    read_uint32(&res->freeBlocks, fd);
    read_uint32(&res->nextAllocation, fd);
    read_uint32(&res->rsrcClumpSize, fd);
    read_uint32(&res->dataClumpSize, fd);

    read_uint32(&res->nextCatalogID, fd);

    read_uint32(&res->writeCount, fd);
    read_uint64(&res->encodingsBitmap, fd);

    for (int i = 0; i < sizeof(res->finderInfo) / SIZE32; ++i) {
        read_uint32(&res->finderInfo[i], fd);
    }

    res->allocationFile = read_fork_data(fd);
    res->extentsFile = read_fork_data(fd);
    res->catalogFile = read_fork_data(fd);
    res->attributesFile = read_fork_data(fd);
    res->startupFile = read_fork_data(fd);
    return res;
}

bool check_reserved_space(const char *buffer, uint32_t length) {
    for (int i = 0; i < length; ++i) {
        if (buffer[i] != NULL_TERM) return false;
    }
    return true;
}

catalog_folder *get_folder_of_value(btree *btree_ptr) {
    catalog_folder *folder = (catalog_folder*) malloc(sizeof(catalog_folder));
    read_int16(&folder->recordType, btree_ptr->fd);
    read_uint16(&folder->flags, btree_ptr->fd);
    read_uint32(&folder->valence, btree_ptr->fd);
    read_uint32(&folder->folderID, btree_ptr->fd);
    read_uint32(&folder->createDate, btree_ptr->fd);
    read_uint32(&folder->contentModDate, btree_ptr->fd);
    read_uint32(&folder->attributeModDate, btree_ptr->fd);
    read_uint32(&folder->accessDate, btree_ptr->fd);
    read_uint32(&folder->backupDate, btree_ptr->fd);

    lseek(btree_ptr->fd, 48, SEEK_CUR);
    read_uint32(&folder->textEncoding, btree_ptr->fd);
    read_uint32(&folder->reserved2, btree_ptr->fd);
    return folder;
}

catalog_file *get_file_of_value(btree *btree_ptr) {
    catalog_file *file = (catalog_file*) malloc(sizeof(catalog_file));
    read_int16(&file->recordType, btree_ptr->fd);
    read_uint16(&file->flags, btree_ptr->fd);
    read_uint32(&file->reserved1, btree_ptr->fd);
    read_uint32(&file->fileID, btree_ptr->fd);
    read_uint32(&file->createDate, btree_ptr->fd);
    read_uint32(&file->contentModDate, btree_ptr->fd);
    read_uint32(&file->attributeModDate, btree_ptr->fd);
    read_uint32(&file->accessDate, btree_ptr->fd);
    read_uint32(&file->backupDate, btree_ptr->fd);

    lseek(btree_ptr->fd, 48, SEEK_CUR);
    read_uint32(&file->textEncoding, btree_ptr->fd);
    read_uint32(&file->reserved2, btree_ptr->fd);

    file->dataFork = *read_fork_data(btree_ptr->fd);
    file->resourceFork = *read_fork_data(btree_ptr->fd);

    return file;
}

hfsplus *read_hfsplus(const char *filename) {
    uint64_t fd = open(filename, O_RDONLY);
    char buffer[RESERVED1];
    read(fd, buffer, RESERVED1);
    if (!check_reserved_space(buffer, RESERVED1)) {
        ERRNO_HFS = NO_RESERVED_SPACE;
        return NULL;
    }
    hfsplus *res = (hfsplus*) malloc(sizeof(hfsplus));
    res->vol_header = read_volume_header(fd);
    uint32_t catalog_file_offset = res->vol_header->blockSize * res->vol_header->catalogFile->extents[0].startBlock;
    res->catalog_file = init_btree(fd, catalog_file_offset);
    catalog_key *key_to_fill = (catalog_key*) malloc(sizeof(catalog_key));
    int32_t read_size = set_value_ptr_by_id(res->catalog_file, ROOT_FOLDER_ID, key_to_fill);
    if (read_size <= 0) {
        ERRNO_HFS = BAD_ROOT_STRUCTURE;
        return NULL;
    }
    res->current_folder_info = get_folder_of_value(res->catalog_file);
    res->current_folder_id = res->current_folder_info->folderID;
    res->path_folder_depth = 1;
    res->pathFolderIds[0] = ROOT_FOLDER_ID;
    res->pathFolderIds[1] = res->current_folder_id;
    res->pwd = key_to_fill->node_name;
    free(key_to_fill);//because key.node_name is in pwd
    return res;
}

char *pwd(hfsplus *fs) {
    return fs->pwd;
}

uint32_t ls(hfsplus *fs, char **buffer) {
    return get_children_of_id(fs->catalog_file, fs->current_folder_id, buffer);
}

char *cd(hfsplus *fs, char *destination) {
    char *part_dest = strtok(destination, "/");
    while (part_dest != NULL) {
        int16_t type;
        int32_t offset = get_value_offset_by_key(part_dest, fs->catalog_file, fs->current_folder_id, &type);
        if (offset < 0) {
            ERRNO_HFS = NO_SUCH_FILE_OR_DIR;
            return NULL;
        }
        if (type != FolderRecord) {
            ERRNO_HFS = IS_FILE;
            return NULL;
        }
        lseek(fs->catalog_file->fd, offset, SEEK_SET);
        fs->current_folder_info = get_folder_of_value(fs->catalog_file);
        fs->current_folder_id = fs->current_folder_info->folderID;
        fs->path_folder_depth++;
        fs->pathFolderIds[fs->path_folder_depth] = fs->current_folder_id;
        strcat(fs->pwd, "/");
        strcat(fs->pwd, part_dest);
        part_dest = strtok(NULL, "/");
    }
    return fs->pwd;
}

char *back(hfsplus *fs) {
    if (fs->path_folder_depth > 1) {
        fs->path_folder_depth--;
        fs->current_folder_id = fs->pathFolderIds[fs->path_folder_depth];
        for (uint32_t i = strlen(fs->pwd) - 1; i >= 0; --i) {
            if (fs->pwd[i] == '/') {
                fs->pwd[i] = NULL_TERM;
                break;
            }
        }
        return fs->pwd;
    } else {
        ERRNO_HFS = ALREADY_ON_TOP;
        return NULL;
    }
}

int32_t copy_file(char *destination, hfsplus *fs, int32_t offset) {
    if (offset < 0) {
        ERRNO_HFS = NO_SUCH_FILE_OR_DIR;
        return -1;
    }
    lseek(fs->catalog_file->fd, offset, SEEK_SET);
    catalog_file *file = get_file_of_value(fs->catalog_file);
    char data[file->dataFork.logicalSize];
    if (file->dataFork.logicalSize > fs->vol_header->blockSize) {
        for (int i = 0; i < 8; ++i) {
            uint32_t data_offset = fs->vol_header->blockSize * (file->dataFork.extents[i].startBlock - 1);
            uint32_t to_read = fs->vol_header->blockSize * file->dataFork.extents[i].blockCount;
            lseek(fs->catalog_file->fd, data_offset, SEEK_SET);
            read(fs->catalog_file->fd, data, to_read);
        }
    } else {
        uint32_t data_offset = fs->vol_header->blockSize * (file->dataFork.extents[0].startBlock);
        lseek(fs->catalog_file->fd, data_offset, SEEK_SET);
        read(fs->catalog_file->fd, data, file->dataFork.logicalSize);
    }

    int32_t dest_fd = open(destination, O_RDWR | O_CREAT, S_IROTH |S_IWOTH|S_IXOTH);
    if (dest_fd != -1) {
        write(dest_fd, data, file->dataFork.logicalSize);
        close(dest_fd);
        return 0;
    } else {
        return -1;
    }
}

int32_t copy_dir(char *destination, hfsplus *fs, int32_t offset) {
    lseek(fs->catalog_file->fd, offset, SEEK_SET);
    catalog_folder *folder = get_folder_of_value(fs->catalog_file);
    mkdir(destination, S_IROTH |S_IWOTH|S_IXOTH);
    char *buffer[100];
    int32_t found = get_children_of_id(fs->catalog_file, folder->folderID, buffer);

    for (int i = 0; i < found; ++i) {
        int16_t type;
        offset = get_value_offset_by_key(buffer[i], fs->catalog_file, folder->folderID, &type);
        char new_path[200] = {0};
        strcat(new_path, destination);
        strcat(new_path, "/");
        strcat(new_path, buffer[i]);
        if (type == FolderRecord) {
            copy_dir(new_path, fs, offset);
        } else {
            copy_file(new_path, fs, offset);
        }
    }
    return 0;
}


int32_t copy(char *name, char *destination, hfsplus *fs) {
    int16_t type;
    int32_t offset = get_value_offset_by_key(name, fs->catalog_file, fs->current_folder_id, &type);
    if (offset == -1) {
        ERRNO_HFS = NO_SUCH_FILE_OR_DIR;
        return -1;
    } else {
        if (type == FolderRecord) {
            return copy_dir( destination, fs, offset);
        } else {
            return copy_file(destination, fs, offset);
        }
    }
}
