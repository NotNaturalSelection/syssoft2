#include <stdio.h>
#include <unistd.h>
#include "header_node.h"
#include "../be_read.h"

#define USER_RECORDS_SIZE 128
#define MAP_RECORD_OFFSET 256

header_record *create_header_record(uint64_t fd) {
    header_record *result = (header_record*) malloc(sizeof(header_record));
    read_uint16(&result->treeDepth, fd);
    read_uint32(&result->rootNode, fd);
    read_uint32(&result->leafRecords, fd);
    read_uint32(&result->firstLeafNode, fd);
    read_uint32(&result->lastLeafNode, fd);
    read_uint16(&result->nodeSize, fd);
    read_uint16(&result->maxKeyLength, fd);
    read_uint32(&result->totalNodes, fd);
    read_uint32(&result->freeNodes, fd);
    read_uint16(&result->reserved1, fd);
    read_uint32(&result->clumpSize, fd);
    read_uint8(&result->btreeType, fd);
    read_uint8(&result->keyCompareType, fd);
    read_uint32(&result->attributes, fd);

    for (int i = 0; i < sizeof(result->reserved3) / sizeof(result->reserved3[0]); ++i) {
        read_uint32(&result->reserved3[i], fd);
    }
    return result;
}

data_node *create_header_node(btree *btree_ptr) {
    data_node *header_node = (data_node*) malloc(sizeof(data_node));
    header_node->descriptor = create_node_descriptor(btree_ptr->fd, btree_ptr->btree_offset);
    header_node->record_pointers = (uint16_t*)  malloc(sizeof(uint16_t) * (header_node->descriptor->numRecords + 1));
    header_node->records = (void**) malloc(sizeof(void *) * header_node->descriptor->numRecords);
    header_node->records[0] = create_header_record(btree_ptr->fd);
    header_node->records[1] = malloc(USER_RECORDS_SIZE);
    read(btree_ptr->fd, header_node->records[1], USER_RECORDS_SIZE);
    uint32_t map_record_size = ((header_record *) header_node->records[0])->nodeSize - MAP_RECORD_OFFSET;
    header_node->records[2] = malloc(map_record_size);
    read(btree_ptr->fd, header_node->records[2], map_record_size);
    return header_node;
}

header_record *get_header_record(btree *btree_ptr) {
    return (header_record *) btree_ptr->header_node->records[0];
}
