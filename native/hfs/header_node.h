#ifndef SOFTWARE1LIB_HEADER_NODE_H
#define SOFTWARE1LIB_HEADER_NODE_H

#include <stdint.h>
#include <stdbool.h>
#include "node.h"
#include "btree.h"

typedef struct  {
    uint16_t    treeDepth;
    uint32_t    rootNode;
    uint32_t    leafRecords;
    uint32_t    firstLeafNode;
    uint32_t    lastLeafNode;
    uint16_t    nodeSize;
    uint16_t    maxKeyLength;
    uint32_t    totalNodes;
    uint32_t    freeNodes;
    uint16_t    reserved1;
    uint32_t    clumpSize;
    uint8_t     btreeType;
    uint8_t     keyCompareType;
    uint32_t    attributes;
    uint32_t    reserved3[16];
} header_record;

data_node *create_header_node(btree *btree_ptr);

header_record *get_header_record(btree* btree_ptr);

#endif //SOFTWARE1LIB_HEADER_NODE_H
