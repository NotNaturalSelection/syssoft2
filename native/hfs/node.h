#ifndef SOFTWARE1LIB_NODE_H
#define SOFTWARE1LIB_NODE_H

#include <stdint.h>
#include <bits/types/FILE.h>
#include <stdlib.h>
#include <stdbool.h>

typedef enum {
    LeafNode = -1,
    IndexNode = 0,
    HeaderNode = 1,
    MapNode = 2
} kind;

typedef struct {
    uint32_t fLink;//forward
    uint32_t bLink;//backward
    int8_t kind;
    uint8_t height;
    uint16_t numRecords;
    uint16_t reserved;
} node_descriptor;

typedef struct {
    node_descriptor *descriptor;
    void **records;
    uint16_t *record_pointers;
} data_node;

node_descriptor *create_node_descriptor(uint64_t fd, uint32_t offset);

void fill_record_pointers(data_node *ptr, uint64_t fd, uint32_t offset);

void fill_records(data_node *ptr, uint64_t fd, uint32_t offset);

void free_node(data_node *ptr);

//bool is_of_type(data_node *node_ptr, kind type);

#endif //SOFTWARE1LIB_NODE_H
