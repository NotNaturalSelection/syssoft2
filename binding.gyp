{
  "targets": [
    {
      "target_name": "nativehfs",
      "sources": ["native/lib.cc", "native/partitions/partition.cc", "native/hfs/btree.cc", "native/hfs/header_node.cc", "native/hfs/hfs.cc", "native/hfs/node.cc", "native/be_read.cc"],
      "include_dirs": [
              "<!(node -e \"require('nan')\")"
      ]
    }
  ]
}